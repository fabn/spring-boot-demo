package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}

@CrossOrigin("*")
@RestController
class Controller {

	private static final Logger logger = LoggerFactory.getLogger(Controller.class);

	@Autowired
	ProductRepository repository;

	@GetMapping("/")
	Map<Object, Object> home() {
		HashMap<Object, Object> m = new HashMap<>();
		m.put("Hello", "From spring modified");
		return m;
	}

  @GetMapping("/products")
  List<Product> getProducts() {
    return repository.findAll();
  }

  @GetMapping("/products/test")
  Map<Object, Object> productsTest() throws UnknownHostException {
	  HashMap<Object, Object> m = new HashMap<>();
	  m.put("Hello from", InetAddress.getLocalHost().getHostName());
	  return m;
  }

	@PostConstruct
	void createProducts() {
		logger.info("Collegato al database, trovati {} prodotti", repository.count());
		if (repository.count() > 0) return;
		logger.info("Creo i prodotti su database vuoto");
		for (int i = 0; i < 5; i++) {
			Product product = new Product();
			product.setName("Prodotto " + (i + 1));
			repository.save(product);
		}
	}
}

@Entity
class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;
	String name;

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}
}

@Repository
interface ProductRepository extends JpaRepository<Product, Long> {

}
