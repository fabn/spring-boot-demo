# Stage iniziale dove si costruisce il file JAR
# Il nome dello "container" e' dato dalla label che passo con as
FROM openjdk:11-slim-buster as build
WORKDIR /workspace/app

# Questo è quello che mi serve per buildare l'app
COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .

# Cache dependencies (impiega circa 45s)
RUN ./mvnw -B dependency:go-offline 

# Copio l'applicazione nella sua interezza
COPY src src

# Creo il jar eseguibile
RUN ./mvnw -B package -DskipTests
# L'ultimo step ha come output il file jar eseguibile che mi copiero' nello stage successivo

# Questa e' la versione che andra' in esecuzione
# usando immagine JRE only e compilando nello stage precedente)
FROM openjdk:11-jre-slim-buster

# Creo una directory di lavoro
RUN mkdir /app

# Mi ci posiziono
WORKDIR /app

# Passaggio chiave per copiare il jar dalla altra stage, posso fare riferimento alla
# Struttura filesystem che ho li
COPY --from=build /workspace/app/target/*.jar /app/app.jar

# Comando da eseguire quando lancio il container
CMD ["java", "-jar", "/app/app.jar"]
