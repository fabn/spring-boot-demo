# Getting Started

Versione 1 con maven

docker build -f with_maven.dockerfile --progress plain -t java-spring:maven .
docker run --rm -p 8080:8080  java-spring:maven

Approccio con compilazione locale

docker build -f local_build.dockerfile --progress plain -t java-spring:local .
docker run --rm -p 8080:8080  java-spring:local

Docker file finale che usa approccio multistage

docker build --progress plain -t java-spring:final .
docker run --rm -p 8080:8080  java-spring:final