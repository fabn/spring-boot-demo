# Usando immagine JRE only e compilando sulla macchina locale (con il comando ./mvnw package -D skipTests)
# Si arriva a 230MB
FROM openjdk:11-jre-slim-buster

# Creo una directory di lavoro
RUN mkdir /app

# Mi ci posiziono
WORKDIR /app

# Questo tipo di build assume che il jar venga prodotto localmente
COPY target/demo-0.0.1-SNAPSHOT.jar /app

# Comando da eseguire quando lancio il container
CMD ["java", "-jar", "/app/demo-0.0.1-SNAPSHOT.jar"]
