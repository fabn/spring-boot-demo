# Parto da immagine con Java
FROM openjdk:18

# Creo una directory di lavoro
RUN mkdir /app

# Mi ci posiziono
WORKDIR /app

# Copio i sorgenti della mia applicazione
COPY . /app

# Installa le dipendenze
RUN ./mvnw compile

# Comando da eseguire quando lancio il container
CMD ["/app/mvnw", "spring-boot:run"]
